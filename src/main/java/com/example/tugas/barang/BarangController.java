package com.example.tugas.barang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/barang")
public class BarangController {
    private final BarangService barangService;

    @Autowired
    public BarangController(BarangService barangService) {
        this.barangService = barangService;
    }

    @GetMapping
    public List<Barang> getBarang(){
        return barangService.getBarang();
    }

    @GetMapping("/{kode}")
    public Object showBarang(@PathVariable("kode") String kode)
    {
        Optional<Barang> ambilBarang = barangService.getBarangByCode(kode);
        if (ambilBarang.isPresent())
            return ambilBarang.get();
        else
            return "Barang tidak ditemukan";
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Object insertBarang(@RequestBody Barang barang){
        return barangService.insertBarang(barang);
    }

    @PutMapping(value = "/update/{kode}")
    public Object updateBarang(@PathVariable("kode") String kode, @RequestBody Barang barangBaru){;
        return barangService.update(kode, barangBaru);
    }

    @DeleteMapping("/delete/{kode}")
    public Object deleteBarang(@PathVariable("kode") String kode){
        return barangService.delete(kode);
    }

    @GetMapping("/berat")
    public List<Barang> showYangBeratnyaLebihKecil(){
        List<Barang> dataBarangSesuaiBerat = barangService.getBarang();
        List<Barang> beratBaru = new ArrayList<Barang>();
        for (Barang barang:dataBarangSesuaiBerat
             ) {
            if(barang.getBerat()<1.0){
                beratBaru.add(barang);
            }
        }
        return beratBaru;
    }

    @GetMapping("/kode")
    public List<Barang> showYangKodenyaDibawah5(){
        List<Barang> dataBarangSesuaiKode = barangService.getBarang();
        List<Barang> kodeBaru = new ArrayList<Barang>();
        for (Barang barang:dataBarangSesuaiKode
             ) {
            if(barang.getKode().length()>5){
                kodeBaru.add(barang);
            }
        }
        return kodeBaru;
    }
}
