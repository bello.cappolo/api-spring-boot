package com.example.tugas.barang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class BarangService {

    private final BarangRepository barangRepository;

    @Autowired
    public BarangService(BarangRepository barangRepository) {
        this.barangRepository = barangRepository;
    }

    public List<Barang> getBarang(){

        return barangRepository.findAll();
    }

    public Optional<Barang> getBarangByCode(String kode){
        return barangRepository.findByKode(kode);
    }

    public Object insertBarang(Barang barangBaru) {
        List<Barang> cekDuplikatKode = barangRepository.findAll();
        for (Barang barang:cekDuplikatKode
             ) {
            if(barang.getKode().equals(barangBaru.getKode())){
                return "Data sudah ada";
            }
        }

        barangRepository.save(barangBaru);
        return barangBaru;
    }

    public Object update(String kode, Barang barangBaru){
        Optional<Barang> ambilBarang = barangRepository.findByKode(kode);
        List<Barang> cekKode = barangRepository.findAll();
        for (Barang barang:cekKode
        ) {
            if(barang.getId()!=ambilBarang.get().getId()){
                if(barang.getKode().equals(barangBaru.getKode())){
                    return "Data sudah ada";
                }
            }
        }

        if (!ambilBarang.isPresent())
            return "Barang tidak ditemukan";

        Barang barangYangAkanDiUpdate = ambilBarang.get();
        barangYangAkanDiUpdate.setKode(barangBaru.getKode());
        barangYangAkanDiUpdate.setNama(barangBaru.getNama());
        barangYangAkanDiUpdate.setBerat(barangBaru.getBerat());
        barangRepository.save(barangYangAkanDiUpdate);

        return barangYangAkanDiUpdate;
    }

    public Object delete(String kode){
        Optional<Barang> listBarang = barangRepository.findByKode(kode);
        if (!listBarang.isPresent())
            return "Barang tidak ditemukan";

        Barang barangYangMauDiHapus = listBarang.get();
        barangRepository.delete(barangYangMauDiHapus);
        return barangYangMauDiHapus;
    }
}
