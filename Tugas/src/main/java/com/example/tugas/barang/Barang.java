package com.example.tugas.barang;

import javax.persistence.*;

@Entity
@Table
public class Barang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    private String kode;
    private String nama;
    private float berat;

    public Barang(){

    }

    public Barang(String kode, String nama, float berat) {
        this.kode = kode;
        this.nama = nama;
        this.berat = berat;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public float getBerat() {
        return berat;
    }

    public void setBerat(float berat) {
        this.berat = berat;
    }

    @Override
    public String toString() {
        return "Barang{" +
                "kode='" + kode + '\'' +
                ", nama='" + nama + '\'' +
                ", berat=" + berat +
                '}';
    }
}
