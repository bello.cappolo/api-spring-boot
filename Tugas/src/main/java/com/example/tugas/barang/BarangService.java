package com.example.tugas.barang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class BarangService {

    private final BarangRepository barangRepository;

    @Autowired
    public BarangService(BarangRepository barangRepository) {
        this.barangRepository = barangRepository;
    }

    public List<Barang> getBarang(){

        return barangRepository.findAll();
    }

    public Optional<Barang> getBarangByCode(String kode){
        return barangRepository.findByKode(kode);
    }

    public Barang postBarang(Barang barang) {
        Barang newBarang = new Barang();
        newBarang.setKode(barang.getKode());
        newBarang.setNama(barang.getNama());
        newBarang.setBerat(barang.getBerat());

        barangRepository.save(newBarang);
        return newBarang;
    }

}
