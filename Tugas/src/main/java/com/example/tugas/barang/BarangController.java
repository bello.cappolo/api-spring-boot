package com.example.tugas.barang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/barang")
public class BarangController {
    private final BarangService barangService;

    @Autowired
    public BarangController(BarangService barangService) {
        this.barangService = barangService;
    }

    @GetMapping
    public List<Barang> getBarang(){
        return barangService.getBarang();
    }

    @GetMapping("/{kode}")
    public Object showBarang(@PathVariable("kode") String kode)
    {
        Optional<Barang> ambilBarang = barangService.getBarangByCode(kode);
        if (ambilBarang.isPresent())
            return ambilBarang.get();
        else
            return "Barang tidak ditemukan";
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Barang insertBarang(@RequestBody Barang barang){
        return barangService.postBarang("A003");
    }

//    @PatchMapping("/{kode}")
//    public List<Barang> getBarang(){
//        return barangService.getBarang();
//    }
//
//    @DeleteMapping("/{kode}")
//    public List<Barang> getBarang(){
//        return barangService.getBarang();
//    }
}
