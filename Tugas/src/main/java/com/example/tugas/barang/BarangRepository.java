package com.example.tugas.barang;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BarangRepository
        extends JpaRepository<Barang, String> {

    Optional<Barang> findByKode(String s);
}
